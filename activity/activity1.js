// 1

db.users.insertMany(

[

	{
		"firstName": "Diane",
		"lastName": "Murphy",
		"email": "dmurphy@gmail.com",
		"isAdmin": false,
		"isActive": true
	},
	{
		"firstName": "Mary",
		"lastName": "Patterson",
		"email": "mpatterson@gmail.com",
		"isAdmin": false,
		"isActive": true
	},
	{
		"firstName": "Jeff",
		"lastName": "Firrelli",
		"email": "jfirrelli@gmail.com",
		"isAdmin": false,
		"isActive": true
	},
	{
		"firstName": "Gerard",
		"lastName": "Bondur",
		"email": "gbondur@gmail.com",
		"isAdmin": false,
		"isActive": true
	},
	{
		"firstName": "Pamela",
		"lastName": "Castillo",
		"email": "pcastillo@gmail.com",
		"isAdmin": true,
		"isActive": false
	},
	{
		"firstName": "George",
		"lastName": "Vanauf",
		"email": "gvanauf@gmail.com",
		"isAdmin": true,
		"isActive": true
	}

]
);

// 2

db.courses.insertMany([

{
	"name": "Professional Development",
	"price": 10000.0,
},
{
	"name": "Business Processing",
	"price": 13000.0,
}

	])

// 3

db.users.find({ "isAdmin": false })

// 4

db.courses.updateOne({
	"name": "Professional Development"
}, {
	$set: {
		"enrollees": [
		{ "userId": ObjectId("620ccedd858d7a42f4727cd2")},
		{ "userId": ObjectId("620ccedd858d7a42f4727cd3")},
		{ "userId": ObjectId("620ccedd858d7a42f4727cd4")}
		]
});

